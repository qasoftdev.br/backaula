import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioCadastroComponent } from './usuario/usuario-cadastro/usuario-cadastro.component'
import { UsuarioListarComponent } from './usuario/usuario-listar/usuario-listar.component'
import { TemplateComponent } from './template/template/template.component';
import { UsuarioDeletarComponent } from './usuario/usuario-deletar/usuario-deletar.component';

const routes: Routes = [
  { path: 'usuario-listar', component: UsuarioListarComponent },
  { path: 'usuario-cadastro', component: UsuarioCadastroComponent},

  { path: '', redirectTo: 'view/usuario-listar', pathMatch: 'full'},

  {
    path: 'view',
    component: TemplateComponent, // this is the component with the <router-outlet> in the template
    children: [
      {
        path: 'usuario-listar', // child route path
        component: UsuarioListarComponent, // child route component that the router renders
      },
      {
        path: 'usuario-cadastro',
        component: UsuarioCadastroComponent, // another child route component that the router renders
      },
      {
        path: 'usuario-cadastro/:id',
        component: UsuarioCadastroComponent, // another child route component that the router renders
      },
      {
        path: 'usuario-deletar/:id',
        component: UsuarioDeletarComponent, // another child route component that the router renders
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
