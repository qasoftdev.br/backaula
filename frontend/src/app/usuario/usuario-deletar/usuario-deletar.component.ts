import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from '../usuario.model';
import { UsuarioService } from '../usuario.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-usuario-deletar',
  templateUrl: './usuario-deletar.component.html',
  styleUrls: ['./usuario-deletar.component.css']
})
export class UsuarioDeletarComponent implements OnInit {

  usuario : UsuarioModel = new UsuarioModel();

  constructor(private usuarioService: UsuarioService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      let id = paramMap.get('id')

      if(id != null) {
        this.usuarioService.buscar(id).subscribe(usuario => {
          this.usuario = usuario

        })
      }
    })
  }

  deletar() {
    console.log(this.usuario.idUsuario)
    this.usuarioService.deletar(this.usuario.idUsuario).subscribe(() => {
      this.router.navigate(['/view/usuario-listar'])
    } )
  }

  cancelar() {
    this.router.navigate(['/view/usuario-listar'])
  }

}
