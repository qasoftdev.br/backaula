import { Component, OnInit } from '@angular/core';
import { UsuarioModule } from '../usuario.module';
import { UsuarioModel } from '../usuario.model';
import { UsuarioService } from '../usuario.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-usuario-cadastro',
  templateUrl: './usuario-cadastro.component.html',
  styleUrls: ['./usuario-cadastro.component.css']
})
export class UsuarioCadastroComponent implements OnInit {

  usuario : UsuarioModel = new UsuarioModel();

  constructor(private usuarioService: UsuarioService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.paramMap.subscribe(paramMap => {
      let id = paramMap.get('id')

      if(id != null) {
        this.usuarioService.buscar(id).subscribe(usuario => {
          this.usuario = usuario
        })
      }
    })
  }

  salvar() {
    this.usuario.admin= false;
    this.usuario.professor = false;
    this.usuarioService.salvar(this.usuario).subscribe(usuario => {
      console.log(this.usuario);
    },
      error => console.log(error),
      () => console.log('Finalizado'));

      this.router.navigate(['/view/usuario-cadastro'])
  }
}
