import { Injectable } from '@angular/core';
import { UsuarioModel } from './usuario.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})



export class UsuarioService {

  constructor( private http: HttpClient) { }


    salvar(usuario: UsuarioModel) {
      return this.http.post<UsuarioModel>
      ('http://localhost:8080/api/usuario', usuario)
    }

    listar(): Observable<UsuarioModel[]> {
      return this.http.get<UsuarioModel[]>('http://localhost:8080/api/usuario');
    }

    buscar(idUsuario: any): Observable<UsuarioModel> {
      return this.http.get<UsuarioModel>
        ('http://localhost:8080/api/usuario/' + idUsuario);
    }

    buscarPorNome(nome: string): Observable<UsuarioModel> {
      return this.http.get<UsuarioModel>
        ('http://localhost:8080/api/usuario/' + nome);
    }

    deletar(idUsuario: number ): Observable<UsuarioModel> {
      return this.http.delete<UsuarioModel>
      ('http://localhost:8080/api/usuario/' + idUsuario);
    }

  }

