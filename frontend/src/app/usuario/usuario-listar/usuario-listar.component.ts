import { Component, OnInit } from '@angular/core';
import { UsuarioModel} from '../usuario.model';

import { UsuarioService } from '../usuario.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-usuario-listar',
  templateUrl: './usuario-listar.component.html',
  styleUrls: ['./usuario-listar.component.css']
})
export class UsuarioListarComponent implements OnInit {

  usuario : UsuarioModel = new UsuarioModel();

  displayedColumns: string[] = ['id', 'nome', 'cpf', 'email', 'whatsapp', 'admin', 'professor', 'editar', 'excluir'];
  dataSource: any;

  // usuarios: UsuarioModel[] = [
  //   { id: 1, nome:'Rodrigo', cpf: '1234567890', email: 'teste@teste.com', whatsapp: '41999442776', admin: true, professor: false},
  //   { id: 2, nome:'Rinaldo', cpf: '1234567890', email: 'teste1@teste.com', whatsapp: '41999442776', admin: true, professor: false},
  //   { id: 3, nome:'Elizeu', cpf: '1234567890', email: 'teste2@teste.com', whatsapp: '41999442776', admin: true, professor: false},
  //   { id: 4, nome:'Ze das Couves', cpf: '1234567890', email: 'teste4@teste.com', whatsapp: '41999442776', admin: true, professor: false},
  //   { id: 5, nome:'Maria Porcina', cpf: '1234567890', email: 'teste5@teste.com', whatsapp: '41999442776', admin: true, professor: false},
  // ]

  // constructor(private usuarioService: UsuarioService) { }
  constructor(private usuarioService: UsuarioService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.usuarioService.listar().subscribe(usuarios => {
      this.dataSource = usuarios;
    },
      error => console.log(error),
      () => console.log('finalizado')
    );

  }

  buscaPorNome() {
    this.route.paramMap.subscribe(paramMap => {
      let nome = paramMap.get('nome')
      console.log(nome)

      if (nome!= null) {
        this.usuarioService.buscarPorNome(nome).subscribe(usuario => {
          this.usuario = usuario;
        },
          error => console.log(error),
          () => console.log('Localizado'));
      }
  })
  }

  voltar() {
    this.router.navigate(['/view/usuario-cadastro'])
  }

}
