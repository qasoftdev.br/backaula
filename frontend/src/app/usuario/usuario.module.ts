import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatIconModule,
  MatTableModule,
  MatButtonModule,
  MatCardModule,
  MatCardTitle
} from '@angular/material';
import { FormsModule, } from '@angular/forms';

import { UsuarioCadastroComponent } from './usuario-cadastro/usuario-cadastro.component';
import { UsuarioListarComponent } from './usuario-listar/usuario-listar.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';
import { UsuarioDeletarComponent } from './usuario-deletar/usuario-deletar.component';


@NgModule({
  declarations: [UsuarioCadastroComponent, UsuarioListarComponent, UsuarioDeletarComponent],
  imports: [
  CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    FormsModule,
    MatTableModule,
    MatButtonModule,
    HttpClientModule,
    AppRoutingModule,
    MatCardModule
  ]
})
export class UsuarioModule { }
