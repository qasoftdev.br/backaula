import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateComponent } from './template/template.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from '../app-routing.module';


@NgModule({
  declarations: [TemplateComponent, FooterComponent, HeaderComponent],
  imports: [
  CommonModule,
  AppRoutingModule
  ]
})
export class TemplateModule { }
