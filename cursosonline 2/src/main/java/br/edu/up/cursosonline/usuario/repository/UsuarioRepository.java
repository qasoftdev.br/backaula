package br.edu.up.cursosonline.usuario.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.up.cursosonline.usuario.entity.Usuario;


public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	//Usuario nome(String nome);
	
	List<Usuario> findByNomeContaining(String nome);

}

