package br.edu.up.cursosonline.usuario.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.up.cursosonline.usuario.entity.Usuario;
import br.edu.up.cursosonline.usuario.repository.UsuarioRepository;
import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepo;

	public List<Usuario> listar() {
		
		return usuarioRepo.findAll() ;
		
	}

	public void salvar(Usuario usuario) {
		
		usuarioRepo.save(usuario);
		
	}

	public void alterar(Usuario usuario) {
		
		usuarioRepo.save(usuario);
		
	}

	public void excluir(Long idUsuario) {
		
		usuarioRepo.deleteById(idUsuario);
		
	}

	public Optional<Usuario> localizarPorId(Long idUsuario) {
		 
		return usuarioRepo.findById(idUsuario);
	
	}

	public List<Usuario> localizarPorNome(String nome) {
		
		List<Usuario> usuarios = usuarioRepo.findByNomeContaining(nome) ;
		
		return usuarios;
	}
	
}