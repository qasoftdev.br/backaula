package br.edu.up.cursosonline.usuario.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.edu.up.cursosonline.usuario.entity.Usuario;
import br.edu.up.cursosonline.usuario.service.UsuarioService;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping
	public List<Usuario> listar() {
		
		return usuarioService.listar();
	}
	
	@PostMapping
	public void salvar(@RequestBody Usuario usuario) {
		
		usuarioService.salvar(usuario);
	}
	
	@PutMapping 
	public void alterar(@RequestBody Usuario usuario) {
		
		usuarioService.alterar(usuario);
	}
	
	@DeleteMapping(path = {"/{idUsuario}"})
	public void excluir(@PathVariable Long idUsuario) {
		
		usuarioService.excluir(idUsuario);
		
	}
	
	@GetMapping(path = {"/{idUsuario}"})
	public Optional<Usuario> localizarPorId(@PathVariable Long idUsuario){
		return usuarioService.localizarPorId(idUsuario);
		
	}
	
	
	
	@GetMapping(path = {"/nome/{nome}"})
	public List<Usuario> localizarPorNome(@PathVariable String nome) {
		return usuarioService.localizarPorNome(nome);
		
	}
	
}